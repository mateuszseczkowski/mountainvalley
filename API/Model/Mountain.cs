using System;
using System.Collections.Generic;

namespace MountainValley
{
    public class Mountain
    {
        public Guid Guid { get; set; }

        public string Name { get; set; }

        public DateTime DateWasDiscovered { get; set; }

        public int Height { get; set; }

        public IEnumerable<string> FamousClimbers;

        public string Summary { get; set; }
    }
}
