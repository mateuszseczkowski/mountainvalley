﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MountainValley.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MountainController : ControllerBase
    {
        private const int NumberOfMountainsToGenerate = 3;

        private readonly ILogger<MountainController> _logger;

        public MountainController(ILogger<MountainController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Mountain> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, NumberOfMountainsToGenerate).Select(index => new Mountain
            {
                Guid = Guid.NewGuid(),
                Name = "MountainName_" + index,
                DateWasDiscovered = DateTime.Today,
                Height = rng.Next(900, 8000),
                Summary = ""
            })
            .ToArray();
        }
    }
}
