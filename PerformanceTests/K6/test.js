import http from "k6/http";
import { sleep } from "k6";

const testRun = true;
const rampUpDuration = testRun ? "2s" : "2m";
const mainStageDuration = testRun ? "3s" : "53m";
const closeDuration = testRun ? "2s" : "2m";
const vus = testRun ? 2 : 90;

export let options = {
  stages: [
    { duration: rampUpDuration, target: vus }, // Ramp up from 0 to x users over ...
    { duration: mainStageDuration, target: vus }, // Stay at x users for ...
    { duration: closeDuration, target: 0 }, // Ramp down from x to 0 users over ...
  ],
  thresholds: {
    http_req_failed: ["rate<0.02"], // http errors should be less than 2%
    http_req_duration: ["p(95)<500"], // 95% of requests should be under 500ms
  },
};

export default function () {
  http.get("http://localhost:5001/Mountain");
  sleep(1);
}
