# AY K6 Load Testing

## Install

Follow the installation steps [here](https://k6.io/docs/get-started/installation/).

## Script run

### Simple

```
k6 run test.js
```

### Exporting both summary and metrics as CSV

```
k6 run --summary-export=out/summary_$(date +"%F_%H%M").json --out csv=out/result_$(date +"%F_%H%M").csv test.js
```
or use
```
./lib/run.sh
```

### Save metrics directly in the local instance of Influxdb

```
k6 run --out influxdb=http://localhost:8086/{db_name} test.js
```

## VM config

[private]

### Crontab

[private]

## InfluxDB

[private]

### Version 1.x vs. version 2.x

[private]

### Docker

To install and setup InfluxDB follow the instructions [here](https://hub.docker.com/_/influxdb).

#### Pull v1.8

Use default configuration

```
docker run -p 8086:8086 \
      -v influxdb:/var/lib/influxdb \
      --name influxdb \
      influxdb:1.8
```

#### Run from container

[private]

## Telegraf

[private]

### Run

[private]

### Limitations

[private]

## Grafana

Grafana can be used to visualize the k6 load tests results.

Read more [here](https://k6.io/docs/results-output/grafana-dashboards/).

### Docker

To install and initial setup Grafana follow the instructions [here](https://grafana.com/docs/grafana/latest/setup-grafana/installation/docker/).

Use default configuration.

### Setup

[private]
