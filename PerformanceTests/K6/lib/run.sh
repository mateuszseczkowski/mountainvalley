#!/bin/sh
#start the k6 test
TESTS_HOME="/home/ubuntu/performance/k6/"
TEST_SCRIPT_NAME="test.js"
TS="$(date +"%F_%H%M")"

echo -e `k6 run ${TESTS_HOME}/${TEST_SCRIPT_NAME} --summary-export=${TESTS_HOME}/out/summary_${TS}.json --out csv=${TESTS_HOME}/out/result_${TS}.csv`